from pytumblr import TumblrRestClient
from requests_oauthlib import OAuth1Session

class TumblrClient():
    def __init__(self, consumer_key, consumer_secret):
        self.consumer_key = consumer_key
        self.consumer_secret = consumer_secret
        self.request_token_url = 'http://www.tumblr.com/oauth/request_token'
        self.authorize_url = 'http://www.tumblr.com/oauth/authorize'
        self.access_token_url = 'http://www.tumblr.com/oauth/access_token'
        self.oauth_session = OAuth1Session(consumer_key, client_secret=consumer_secret)
        fetch_response = self.oauth_session.fetch_request_token(self.request_token_url)
        self.resource_owner_key = fetch_response.get('oauth_token')
        self.resource_owner_secret = fetch_response.get('oauth_token_secret')

    def get_full_auth_url(self):
        return self.oauth_session.authorization_url(self.authorize_url)

    def parse_redirect_url(self, redirect_response: str):
        oauth_response = self.oauth_session.parse_authorization_response(redirect_response)
        self.verifier = oauth_response.get('oauth_verifier')
        oauth_session = OAuth1Session(
        self.consumer_key,
        client_secret=self.consumer_secret,
        resource_owner_key=self.resource_owner_key,
        resource_owner_secret=self.resource_owner_secret,
        verifier=self.verifier
        )
        oauth_tokens = oauth_session.fetch_access_token(self.access_token_url)

        self.tokens = {
            'consumer_key': self.consumer_key,
            'consumer_secret': self.consumer_secret,
            'oauth_token': oauth_tokens.get('oauth_token'),
            'oauth_token_secret': oauth_tokens.get('oauth_token_secret')
        }
    def create_client(self):
        oauth_session = OAuth1Session(
            self.consumer_key,
            client_secret=self.consumer_secret,
            resource_owner_key=self.resource_owner_key,
            resource_owner_secret=self.resource_owner_secret,
            verifier=self.verifier
        )
        oauth_tokens = oauth_session.fetch_access_token(self.access_token_url)
        self.client = TumblrRestClient(
        self.tokens['consumer_key'],
        self.tokens['consumer_secret'],
        oauth_tokens['oauth_token'],
        oauth_tokens['oauth_token_secret']
    )
        self.info = self.client.info()
    
    def create_text_post(self, title, body):
        blog_name = self.info["user"]["blogs"][0]["name"]
        return self.client.create_text(blog_name, title=title, body=body)
        
    
